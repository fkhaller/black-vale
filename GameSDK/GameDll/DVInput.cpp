#include "StdAfx.h"
#include "DVInput.h"

CDVInput::CDVInput()
{
	gEnv->pGame->GetIGameFramework()->GetIActionMapManager()->AddExtraActionListener(this);
	if (m_actionHandler.GetNumHandlers() == 0)
	{
#define ADD_HANDLER(action, func) m_actionHandler.AddHandler(actions.action, &CDVInput::func)
		const CGameActions& actions = g_pGame->Actions();
		//Inventory Handlers
		ADD_HANDLER(backpack, OnActionBackpack);
		ADD_HANDLER(switchMainPocket, OnActionSwitchMain);
		ADD_HANDLER(switchLeftPocket, OnActionSwitchLeft);
		ADD_HANDLER(switchFrontPocket, OnActionSwitchFront);
		ADD_HANDLER(switchRightPocket, OnActionSwitchRight);
		ADD_HANDLER(nextInv, OnActionNextItem);
		ADD_HANDLER(prevInv, OnActionPrevItem);
		ADD_HANDLER(removeItem, OnActionRemoveItem);
		ADD_HANDLER(storeItem, OnActionStoreItem);
		ADD_HANDLER(useItemPrimary, OnActionUseItem);
		ADD_HANDLER(useItemSecondary, OnActionUseItem);
#undef ADD_HANDLER
	}
}

bool CDVInput::OnActionBackpack(EntityId entityId, const ActionId& actionId, int activationMode, float value)
{
	if (m_leaveEndTime == 0.f && m_enterEndTime == 0.f && m_switchEndTime == 0.f)
	{
		//set up the action that is needed
		if (!isOpen)
		{
			m_currentPocket = "Main";
			switchBPMap(true);
			//init timer to turn on isIdle at the right point
			m_enterEndTime = gEnv->pTimer->GetFrameStartTime() + m_enterTime;

			m_actionController->Queue(*m_enterInventory);
			m_idleInventory->ToggleFlag(BIT(2));          //Interruptable
			m_actionController->Queue(*m_idleInventory);
			isEnterInvCam = true;

			initPocket();
		}
		else
		{
			setDefaultMaterial();
			//initialize timer to exit inv (switch actionmap) at correct time
			m_leaveEndTime = gEnv->pTimer->GetFrameStartTime() + m_leaveTime;

			//make sure that isIdle gets turned off
			m_idleInventory->ToggleFlag(BIT(9));

			m_idleInventory->ForceFinish();//Toggles off interuptable
			m_actionController->Queue(*m_leaveInventory);
			isEnterInvCam = false;
			isExitInvCam = true;
			isIdle = false;
		}

		isOpen = !isOpen;
	}
	return false;
}

bool CDVInput::OnActionSwitchLeft(EntityId entityId, const ActionId& actionId, int activationMode, float value)
{
	switchPocket("Left");
	return false;
}

bool CDVInput::OnActionSwitchRight(EntityId entityId, const ActionId& actionId, int activationMode, float value)
{
	switchPocket("Right");
	return false;
}

bool CDVInput::OnActionSwitchFront(EntityId entityId, const ActionId& actionId, int activationMode, float value)
{
	switchPocket("Front");
	return false;
}

bool CDVInput::OnActionSwitchMain(EntityId entityId, const ActionId& actionId, int activationMode, float value)
{
	switchPocket("Main");
	return false;
}

bool CDVInput::OnActionNextItem(EntityId entityId, const ActionId& actionId, int activationMode, float value)
{
	setDefaultMaterial();
	nextItem();
	return false;
}

bool CDVInput::OnActionPrevItem(EntityId entityId, const ActionId& actionId, int activationMode, float value)
{
	setDefaultMaterial();
	prevItem();
	return false;
}

bool CDVInput::OnActionRemoveItem(EntityId entityId, const ActionId& actionId, int activationMode, float value)
{
	//Fuel is used from Inventory is not a selectable item
	if (m_currentItem == Batteries)
		if (m_itemList[Flashlight] == m_pPlayer->GetCurrentItemId() && m_batteryAmount != 0)
		{
			m_batteryAmount--;
			m_flashlightLife = 500;
		}
	//you can only be holding 3 items
		else if (m_itemCount < 4)
		{
			//hide proxy
			hideProxyItem(true);

			//add the item to inventory 
			if (!checkPocketEmpty())
			{
				IItem *currItem = g_pGame->GetIGameFramework()->GetIItemSystem()->GetItem(m_itemList[m_currentItem]);
				currItem->PickUp(m_pPlayer->GetEntityId(), false);
				setDefaultMaterial();

				m_backpack &= ~(1 << m_currentItem);

				//go to next item in pocket
				nextItem();
			}

			//unhide proxy
			hideProxyItem(false);

			m_itemCount++;
		}

	return false;
}

bool CDVInput::OnActionStoreItem(EntityId entityId, const ActionId& actionId, int activationMode, float value)
{
	//This will only work for items that started in your
	//backpack for now.
	if (m_itemCount != 0)
	{
		IItem *heldItem = m_pPlayer->GetCurrentItem();

		hideProxyItem(true);

		heldItem->Drop();

		//add flag to backpack and update.
		for (int i = 0; i < 13; i++)
			if (m_itemList[i] == heldItem->GetEntityId())
			{
				m_backpack |= 1 << i;
				updateItem(i);
			}

		m_pPlayer->GetCurrentItem()->AttachToHand(false);

		m_itemCount--;
	}
	return false;
}

bool CDVInput::OnActionUseItem(EntityId entityId, const ActionId& actionId, int activationMode, float value)
{

	if (g_pGame->Actions().useItemPrimary == actionId)
	{
		for (int i = 0; i < 13; i++)
			if (m_itemList[i] == m_pPlayer->GetCurrentItemId())
			{
				useItemPrimary(i, activationMode);
				break;
			}
	}
	else if (g_pGame->Actions().useItemSecondary == actionId)
	{
		for (int i = 0; i < 13; i++)
			if (m_itemList[i] == m_pPlayer->GetCurrentItemId())
			{
				useItemSecondary(i, activationMode);
				break;
			}
	}
	return false;
}