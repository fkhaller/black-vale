#ifndef __DVINPUT_H__
#define __DVINPUT_H__

#pragma once

#include "IActionMapManager.h"

class CDVInput : public IActionListener
{
public:
	CDVInput();

private:
	TActionHandler<CDVInput> m_actionHandler;

	virtual void PreUpdate();
	virtual void Update();
	virtual void PostUpdate();

	virtual void OnAction(const ActionId& action, int activationMode, float value);

public:
	bool OnActionBackpack(EntityId entityId, const ActionId& actionId, int activationMode, float value);
	bool OnActionSwitchLeft(EntityId entityId, const ActionId& actionId, int activationMode, float value);
	bool OnActionSwitchRight(EntityId entityId, const ActionId& actionId, int activationMode, float value);
	bool OnActionSwitchFront(EntityId entityId, const ActionId& actionId, int activationMode, float value);
	bool OnActionSwitchMain(EntityId entityId, const ActionId& actionId, int activationMode, float value);
	bool OnActionNextItem(EntityId entityId, const ActionId& actionId, int activationMode, float value);
	bool OnActionPrevItem(EntityId entityId, const ActionId& actionId, int activationMode, float value);
	bool OnActionRemoveItem(EntityId entityId, const ActionId& actionId, int activationMode, float value);
	bool OnActionStoreItem(EntityId entityId, const ActionId& actionId, int activationMode, float value);
	bool OnActionUseItem(EntityId entityId, const ActionId& actionId, int activationMode, float value);

	void switchBPMap(bool);
};

#endif // !__DVInput_H__
