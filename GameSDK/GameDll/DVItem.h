#ifndef __DVITEM_H__
#define __DVITEM_H__

#pragma once 

#include "Item.h"

class CDVItem : public CItem
{
	//Override (add)
	virtual bool Init(IGameObject * pGameObject);
	virtual void Update(SEntityUpdateContext& ctx, int);

public:
	
};

#endif