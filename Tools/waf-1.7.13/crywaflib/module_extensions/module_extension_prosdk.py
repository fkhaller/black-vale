#############################################################################
## Crytek Source File
## Copyright (C) 2015, Crytek Studios
##
## Creator: Nico Moss
## Date: July 03, 2015
## Description: WAF  based build system
#############################################################################

import os, ConfigParser, subprocess
from waflib import Task
from waflib.TaskGen import feature
from waflib.CryModuleExtension import module_extension
 

@module_extension('prosdk')
def module_extensions_prosdk(ctx, kw, entry_prefix, platform, configuration):	
	kw[entry_prefix + 'defines'] += [ 'IS_PROSDK' ]

	# SolidShield protection applies to the Editor, which is only built for win_x64.
	if platform == 'win_x64' and kw['target'] == 'CrySystem':
		kw[entry_prefix + 'features'] += [ 'solidshield_drm' ]


@feature('solidshield_drm')
def solidshield_drm(self):
	bld           = self.bld
	platform      = bld.env['PLATFORM']
	configuration = bld.env['CONFIGURATION']

	if platform != 'win_x64':
		return

	if configuration != 'profile':
		return

	if platform == 'project_generator' or not bld.options.project_spec: # only support build that have a project spec
		return

	if not getattr(self, 'link_task', None):
		return

	# Create a temporary configuration file by working out the path to CrySystem.dll
	# rather than hard-coding it in the config file.
	dllnode = bld.get_output_folders(platform, configuration)[0].make_node('CrySystem.dll')
	confignode = self.bld.get_bintemp_folder_node().make_node('solidshield.cfg')

	self.create_task('apply_solidshield_drm', dllnode, confignode )


class apply_solidshield_drm(Task.Task):
	command = ''
	output_dir = None

	def runnable_status(self):
		"""
		Wait for dependent tasks to be executed, then read the
		files to update the list of inputs.
		"""
		for t in self.run_after:
			if not t.hasrun:
				return Task.ASK_LATER

		if not os.path.isfile(self.inputs[0].abspath()):
			return Task.ASK_LATER

		return super(apply_solidshield_drm, self).runnable_status()

	def run(self):
		base_cfg = os.path.normpath('Code/Tools/SolidShield/ProSDK_win_x64.ini')
		temp_cfg = self.outputs[0].abspath()

		dllpath = self.inputs[0].abspath()
		dll_parentdir = os.sep.join(dllpath.split(os.sep)[:-1])

		# The solidshield config file contains the paths for both the DLL to be protected
		# and its final path when the DRM has been applied.
		config = ConfigParser.ConfigParser()
		config.read(base_cfg)
		config.set('CONFIG', 'file1', dllpath)
		config.set('CONFIG', 'outputpath', dll_parentdir)

		with open(temp_cfg, 'w') as fp:
			config.write(fp)

		solidshield_exe = os.path.normpath('Code/Tools/SolidShield/sclp-win-i386.exe')
		solidshield_config = os.path.normpath(temp_cfg)

		# Apply the DRM itself - this uploads the DLL to SolidShield, who apply then DRM,
		# then download the protected version to the outputput read above.
		return subprocess.check_call([solidshield_exe, solidshield_config])
