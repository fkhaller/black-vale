// Copyright 2001-2015 Crytek GmbH. All rights reserved.
//
// Head Mounted Display Manager Interface
// This interface is still evolving as more HMDs are supported
// - 03/06/2015 Created by Dario Sancho

#pragma once

#include <IHMDDevice.h>

struct IHMDManager
{
public:

	virtual ~IHMDManager() {}

	enum EHmdSetupAction
	{
		eHSA_CreateCvars = 0,
		eHSA_PreInit,
		eHSA_Init,
	};

	enum EHmdAction
	{
		eHA_DrawInfo = 0,
	};

	struct SAsymmetricCameraSetupInfo
	{
		float fov, aspectRatio, asymH, asymV, eyeDist;
	};

	// basic functionality needed to setup and destroy an HMD during system init / system shutdown
	virtual void SetupAction(EHmdSetupAction cmd) = 0;

	// trigger an action on the current HMD
	virtual void Action(EHmdAction action) = 0;

	// update the tracking information
	virtual void UpdateTracking(EVRComponent vrComponent) = 0;

	// returns the active HMD (or a null pointer if none has been activated)
	virtual IHMDDevice* GetHMDDevice() const = 0;

	// returns true if we have an HMD device recognized and r_stereodevice, r_stereooutput and r_stereomode are properly set for stereo rendering
	virtual bool IsStereoSetupOk() const = 0;

	// populates o_info with the asymmetric camera information returned by the current HMD device
	virtual bool GetAsymmetricCameraSetupInfo(int nEye, SAsymmetricCameraSetupInfo & outInfo) const = 0;
};

