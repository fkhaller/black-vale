// Copyright 2001-2015 Crytek GmbH. All rights reserved.

#pragma once

#include "Cry_Math.h"
#include "BaseTypes.h"
#include "smartptr.h"

#if __cplusplus >= 201103L
#if !defined(DELETE_DEFAULT_CONSTRUCTOR)
#define DELETE_DEFAULT_CONSTRUCTOR(type_name)\
private:\
	type_name() = delete;
#endif // !DELETE_DEFAULT_CONSTRUCTOR

#if !defined(PREVENT_OBJECT_COPY)
#define PREVENT_OBJECT_COPY(type_name)\
private:\
	type_name(type_name const&) = delete;\
	type_name& operator = (type_name const&) = delete;
#endif // !PREVENT_OBJECT_COPY

#else

#if !defined(DELETE_DEFAULT_CONSTRUCTOR)
#define DELETE_DEFAULT_CONSTRUCTOR(class_name)\
private:
#endif // !DELETE_DEFAULT_CONSTRUCTOR

#if !defined(PREVENT_OBJECT_COPY)
#define PREVENT_OBJECT_COPY(type_name)\
private:\
	type_name(type_name const&);\
	type_name& operator = (type_name const&);
#endif // !PREVENT_OBJECT_COPY
#endif // __cplusplus

typedef uint32 TATLIDType;

typedef TATLIDType TAudioObjectID;
#define INVALID_AUDIO_OBJECT_ID ((TAudioObjectID)(0))
#define GLOBAL_AUDIO_OBJECT_ID ((TAudioObjectID)(1))
typedef TATLIDType TAudioControlID;
#define INVALID_AUDIO_CONTROL_ID ((TAudioControlID)(0))
typedef TATLIDType TAudioSwitchStateID;
#define INVALID_AUDIO_SWITCH_STATE_ID ((TAudioSwitchStateID)(0))
typedef TATLIDType TAudioEnvironmentID;
#define INVALID_AUDIO_ENVIRONMENT_ID ((TAudioEnvironmentID)(0))
typedef TATLIDType TAudioPreloadRequestID;
#define INVALID_AUDIO_PRELOAD_REQUEST_ID ((TAudioPreloadRequestID)(0))
typedef TATLIDType TAudioStandaloneFileID;
#define INVALID_AUDIO_STANDALONE_FILE_ID ((TAudioStandaloneFileID)(0))
typedef TATLIDType TAudioEventID;
#define INVALID_AUDIO_EVENT_ID ((TAudioEventID)(0))
typedef TATLIDType TAudioFileEntryID;
#define INVALID_AUDIO_FILE_ENTRY_ID ((TAudioFileEntryID)(0))
typedef TATLIDType TAudioTriggerImplID;
#define INVALID_AUDIO_TRIGGER_IMPL_ID ((TAudioTriggerImplID)(0))
typedef TATLIDType TAudioTriggerInstanceID;
#define INVALID_AUDIO_TRIGGER_INSTANCE_ID ((TAudioTriggerInstanceID)(0))
typedef TATLIDType TATLEnumFlagsType;
#define INVALID_AUDIO_ENUM_FLAG_TYPE ((TATLEnumFlagsType)(0))
#define ALL_AUDIO_REQUEST_SPECIFIC_TYPE_FLAGS ((TATLEnumFlagsType)(0xFFFFFFFF))
typedef TATLIDType TAudioProxyID;
#define INVALID_AUDIO_PROXY_ID ((TAudioProxyID)(0))
#define DEFAULT_AUDIO_PROXY_ID ((TAudioProxyID)(1))

class CAudioObjectTransformation
{
public:
	CAudioObjectTransformation()
		: m_position(ZERO)
		, m_forward(Vec3Constants<float>::fVec3_OneY)
		, m_up(Vec3Constants<float>::fVec3_OneZ)
	{}

	CAudioObjectTransformation(Vec3 const& position)
		: m_position(position)
		, m_forward(Vec3Constants<float>::fVec3_OneY)
		, m_up(Vec3Constants<float>::fVec3_OneZ)
	{}

	CAudioObjectTransformation(Matrix34 const& transformation)
		: m_position(transformation.GetColumn3())
		, m_forward(transformation.GetColumn1()) //assuming forward vector = (0,1,0), also assuming unscaled
		, m_up(transformation.GetColumn2()) //assuming up vector = (0,0,1)
	{
		m_forward.NormalizeFast();
		m_up.NormalizeFast();
	}

	ILINE Vec3 const& GetPosition() const {return m_position;}
	ILINE Vec3 const& GetForward() const {return m_forward;}
	ILINE Vec3 const& GetUp() const {return m_up;}
	
	bool IsEquivalent(Matrix34 const& transformation, float const fEpsilon = VEC_EPSILON) const
	{
		return m_position.IsEquivalent(transformation.GetColumn3(), fEpsilon) &&
		m_forward.IsEquivalent(transformation.GetColumn1(), fEpsilon) &&
		m_up.IsEquivalent(transformation.GetColumn2(), fEpsilon);
	}

private:

	Vec3 m_position;
	Vec3 m_forward;
	Vec3 m_up;
};

#define AUDIO_TRIGGER_IMPL_ID_NUM_RESERVED 100 // IDs below that value are used for the CATLTriggerImpl_Internal

#define	MAX_AUDIO_CONTROL_NAME_LENGTH 64
#define	MAX_AUDIO_FILE_NAME_LENGTH    128
#define	MAX_AUDIO_FILE_PATH_LENGTH    256
#define	MAX_AUDIO_OBJECT_NAME_LENGTH  256
#define	MAX_AUDIO_MISC_STRING_LENGTH  512

enum EAudioRequestFlags : TATLEnumFlagsType
{
	eARF_NONE                   = 0,
	eARF_PRIORITY_NORMAL        = BIT(0),
	eARF_PRIORITY_HIGH          = BIT(1),
	eARF_EXECUTE_BLOCKING       = BIT(2),
	eARF_SYNC_CALLBACK          = BIT(3),
	eARF_SYNC_FINISHED_CALLBACK = BIT(4),
	eARF_STAY_IN_MEMORY         = BIT(5),
	eARF_THREAD_SAFE_PUSH       = BIT(6),
};

enum EAudioRequestType : TATLEnumFlagsType
{
	eART_NONE                           = 0,
	eART_AUDIO_MANAGER_REQUEST          = 1,
	eART_AUDIO_CALLBACK_MANAGER_REQUEST = 2,
	eART_AUDIO_OBJECT_REQUEST           = 3,
	eART_AUDIO_LISTENER_REQUEST         = 4,
	eART_AUDIO_ALL_REQUESTS             = 0xFFFFFFFF,
};

enum EAudioRequestResult : TATLEnumFlagsType
{
	eARR_NONE    = 0,
	eARR_SUCCESS = 1,
	eARR_FAILURE = 2,
};

struct SAudioRequestDataBase
{
	explicit SAudioRequestDataBase(EAudioRequestType const eType)
		: eRequestType(eType)
	{}

	virtual ~SAudioRequestDataBase() {}

	EAudioRequestType const eRequestType;

	DELETE_DEFAULT_CONSTRUCTOR(SAudioRequestDataBase);
	PREVENT_OBJECT_COPY(SAudioRequestDataBase);
};

struct SAudioCallBackInfo
{
	SAudioCallBackInfo(SAudioCallBackInfo const& rOther)
		: pObjectToNotify(rOther.pObjectToNotify)
		, pUserData(rOther.pUserData)
		, pUserDataOwner(rOther.pUserDataOwner)
		, nRequestFlags(rOther.nRequestFlags)
	{}

	explicit SAudioCallBackInfo(
		void* const pPassedObjectToNotify = nullptr,
		void* const pPassedUserData = nullptr,
		void* const pPassedUserDataOwner = nullptr,
		TATLEnumFlagsType const nPassedRequestFlags = eARF_PRIORITY_NORMAL)
		: pObjectToNotify(pPassedObjectToNotify)
		, pUserData(pPassedUserData)
		, pUserDataOwner(pPassedUserDataOwner)
		, nRequestFlags(nPassedRequestFlags)
	{}

	static SAudioCallBackInfo const& GetEmptyObject() { static SAudioCallBackInfo const emptyInstance; return emptyInstance; }

	void* const             pObjectToNotify;
	void* const             pUserData;
	void* const             pUserDataOwner;
	TATLEnumFlagsType const nRequestFlags;
};

struct SAudioRequest
{
	SAudioRequest()
		: nFlags(eARF_NONE)
		, nAudioObjectID(INVALID_AUDIO_OBJECT_ID)
		, pOwner(nullptr)
		, pUserData(nullptr)
		, pUserDataOwner(nullptr)
		, pData(nullptr)
	{}

	~SAudioRequest() {}

	TATLEnumFlagsType      nFlags;
	TAudioObjectID         nAudioObjectID;
	void*                  pOwner;
	void*                  pUserData;
	void*                  pUserDataOwner;
	SAudioRequestDataBase* pData;

	PREVENT_OBJECT_COPY(SAudioRequest);
};

struct SAudioRequestInfo
{
	explicit SAudioRequestInfo(
		EAudioRequestResult const _requestResult,
		void* const _pOwner,
		void* const _pUserData,
		void* const _pUserDataOwner,
		char const* const _szValue,
		EAudioRequestType const _audioRequestType,
		TATLEnumFlagsType const _specificAudioRequest,
		TAudioControlID const _audioControlID,
		TAudioObjectID const _audioObjectID,
		TAudioStandaloneFileID const _audioStandaloneFileID,
		TAudioEventID const _audioEventID)
		: requestResult(_requestResult)
		, pOwner(_pOwner)
		, pUserData(_pUserData)
		, pUserDataOwner(_pUserDataOwner)
		, szValue(_szValue)
		, audioRequestType(_audioRequestType)
		, specificAudioRequest(_specificAudioRequest)
		, audioControlID(_audioControlID)
		, audioObjectID(_audioObjectID)
		, audioStandaloneFileID(_audioStandaloneFileID)
		, audioEventID(_audioEventID)
	{}

	EAudioRequestResult const    requestResult;
	void* const                  pOwner;
	void* const                  pUserData;
	void* const                  pUserDataOwner;
	char const* const            szValue;
	EAudioRequestType const      audioRequestType;
	TATLEnumFlagsType	const      specificAudioRequest;
	TAudioControlID const        audioControlID;
	TAudioObjectID const         audioObjectID;
	TAudioStandaloneFileID const audioStandaloneFileID;
	TAudioEventID const          audioEventID;
	
	DELETE_DEFAULT_CONSTRUCTOR(SAudioRequestInfo);
	PREVENT_OBJECT_COPY(SAudioRequestInfo);
};
