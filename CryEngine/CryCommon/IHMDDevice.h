// Copyright 2001-2015 Crytek GmbH. All rights reserved.
//
// Description: Common interface for head mounted display (HMD) devices and controllers
// - 2014       Created by Carsten Wenzel
// - 03/06/2015 Oculus support by Dario Sancho & Valerio Guagliumi

#pragma once

#include <IInput.h>

enum HMDClass
{
	HMDCls_Oculus
};
	
enum HMDType
{
	HMD_Unknown,
	// OCULUS
	HMD_DK1,
	HMD_DKHD,
	HMD_DK2,
	HMD_CB,
	HMD_EVT
};

enum HMDStatus
{
	HS_OrientationTracked = 0x0001,
	HS_PositionTracked = 0x0002,
	HS_CameraPoseTracked = 0x0004,
	HS_PositionConnected = 0x0008,
	HS_HmdConnected = 0x0010,
	//HS_ValidReferencePosition = 0x0020,

	HS_IsUsable = HS_HmdConnected | HS_OrientationTracked
};

struct HMDDeviceInfo
{
	HMDDeviceInfo()
		: type(HMD_Unknown)
		, productName(0)
		, manufacturer(0)
		, screenWidth(0)
		, screenHeight(0)
		, fovH(0)
		, fovV(0)
	{
	}

	HMDType type;

	const char* productName;
	const char* manufacturer;

	unsigned int screenWidth;
	unsigned int screenHeight;

	float fovH;
	float fovV;
};

enum class EHmdSocialScreen
{
	Off = -1,
	DistortedDualImage = 0,
	UndistortedLeftEye,
	UndistortedRightEye,

	// helpers
	FirstInvalidIndex
};

struct HMDPoseState
{
	HMDPoseState()
		: orientation(Quat::CreateIdentity())
		, position(ZERO)
		, angularVelocity(ZERO)
		, linearVelocity(ZERO)
		, angularAcceleration(ZERO)
		, linearAcceleration(ZERO)
	{
	}

	Quat orientation;
	Vec3 position;
	Vec3 angularVelocity;
	Vec3 linearVelocity;
	Vec3 angularAcceleration;
	Vec3 linearAcceleration;
};

struct HMDTrackingState
{
	HMDTrackingState()
		: pose()
		, statusFlags(0)
	{
	}

	bool CheckStatusFlags(unsigned int checked, unsigned int wanted) const { return (statusFlags & checked) == wanted; }
	bool CheckStatusFlags(unsigned int checkedAndWanted) const { return CheckStatusFlags(checkedAndWanted, checkedAndWanted); }

	HMDPoseState pose;

	unsigned int statusFlags;
};

enum EVRComponent
{
	eVRComponent_Hmd        = BIT(0),
	eVRComponent_Controller = BIT(1),
	eVRComponent_All        = eVRComponent_Hmd | eVRComponent_Controller
};

enum EHmdController
{
	// Oculus
	eHmdController_OculusLeftHand  = 0,
	eHmdController_OculusRightHand = 1,
	eHmdController_MaxNumOculusControllers = 2,
};

struct IHmdController
{
	typedef uint32 TLightColor;

	enum ECaps
	{
		eCaps_Buttons       = BIT(0),
		eCaps_Tracking      = BIT(1),
		eCaps_Sticks        = BIT(2),
		eCaps_Capacitors    = BIT(3),
		eCaps_Gestures      = BIT(4),
		eCaps_ForceFeedback = BIT(5),
		eCaps_Color         = BIT(6),
	};

	virtual bool  IsConnected (EHmdController id) const = 0;

	// returns the state of the current buttons/triggers/gestures 
	// Note:
	// in general action listeners should be used instead of calling directly this functions
	// these functions are still used in FG nodes used for quick prototyping by design
	// TODO: refactor FG node to keep track of the button state internally w/o using this functions
	virtual bool  IsButtonPressed   (EHmdController id, EKeyId button)  const = 0;
	virtual bool  IsButtonTouched   (EHmdController id, EKeyId button)  const = 0;
	virtual bool  IsGestureTriggered(EHmdController id, EKeyId gesture) const = 0;
	virtual float GetTriggerValue   (EHmdController id, EKeyId trigger) const = 0;
	virtual Vec2  GetThumbStickValue(EHmdController id, EKeyId stick)   const = 0;

	// returns the tracking state in the HMDs internal coordinates system
	virtual const HMDTrackingState& GetNativeTrackingState(EHmdController controller)      const = 0;
	// returns the tracking state in the HMD's local tracking space using CRYENGINE's coordinate system
	virtual const HMDTrackingState& GetLocalTrackingState(EHmdController controller) const = 0;

	// applies a signal to the force-feedback motors of the selected controller
	virtual void ApplyForceFeedback(EHmdController id, float freq, float amplitude) = 0;

	// Access to the light color of the some controllers
	virtual void SetLightColor(EHmdController id, TLightColor color) = 0;
	virtual TLightColor GetControllerColor(EHmdController id) const = 0;

	// returns the capabilities of the selected controller
	virtual uint32 GetCaps(EHmdController id) const = 0;
	
protected:
	virtual ~IHmdController() {}
};

struct IHMDDevice
{
	enum EInternalUpdate
	{
		e_DebugInfo = 0,
	};

	virtual void AddRef() = 0;
	virtual void Release() = 0;

	virtual HMDClass GetClass() const = 0;
	virtual void GetDeviceInfo(HMDDeviceInfo& info) const = 0;

	virtual void GetCameraSetupInfo(float& fov, float& aspectRatioFactor) const = 0;
	virtual void GetAsymmetricCameraSetupInfo(int nEye, float& fov, float& aspectRatio, float& asymH, float& asymV, float& eyeDist) const = 0;

	virtual void UpdateInternal(EInternalUpdate) = 0;
	virtual void RecenterPose() = 0;
	virtual void UpdateTrackingState(EVRComponent) = 0;
	
	// returns the tracking state in HMD's internal coordinates system
	virtual const HMDTrackingState& GetNativeTrackingState() const = 0;
	// returns the tracking state in the HMD's local tracking space using CRYENGINE's coordinate system
	virtual const HMDTrackingState& GetLocalTrackingState() const = 0;

	virtual const IHmdController* GetController() const = 0;
	virtual const EHmdSocialScreen GetSocialScreenType() const = 0;
	
protected:
	virtual ~IHMDDevice() {}
};

//////////////////////////////////////////////////////////////////////////
// vendor specifics

namespace CryVR
{

#ifndef EXCLUDE_OCULUS_SDK

namespace Oculus {

struct TextureDesc
{
	uint32 width;
	uint32 height;
	uint32 format;
};

struct TextureSet
{
	void* deviceSwapTextureSet;
	uint32 numTextures;
	void** textures;

	TextureSet()
		: deviceSwapTextureSet(NULL)
		, numTextures(0)
		, textures(NULL)
	{
	}
};

struct Texture
{
	void* deviceTexture;
	void* texture;

	Texture()
		: deviceTexture(NULL)
		, texture(NULL)
	{
	}
};

struct EyeTarget
{
	void* deviceSwapTextureSet;
	Vec2i viewportPosition;
	Vec2i viewportSize;
};

struct IOculusDevice : public IHMDDevice
{
public:
	virtual bool CreateSwapTextureSetD3D11(void* d3d11Device, TextureDesc desc, TextureSet* set) = 0;
	virtual bool CreateMirrorTextureD3D11(void* d3d11Device, TextureDesc desc, Texture* texture) = 0;
	virtual void DestroySwapTextureSet(TextureSet* set) = 0;
	virtual void DestroyMirrorTexture(Texture* texture) = 0;
	virtual void PrepareTexture(TextureSet* set, uint32 frameIndex) = 0;
	virtual void SubmitFrame(EyeTarget leftEye, EyeTarget rightEye) = 0;
protected:
	virtual ~IOculusDevice() {}
};

} // namespace Oculus

#endif // #ifndef EXCLUDE_OCULUS_SDK
} // namespace CryVR
