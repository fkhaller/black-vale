#include "Backpack.h"
#include <IEntity.h>
#include "LevelSystem.h"

Backpack::Backpack()
{
	//ToDo serialize the uint32s' on save and grab there values here
	//for initialization
	m_mainPocket = 0;
	m_leftPocket = 0;
	m_rightPocket= 0;
	m_frontPocket= 0;

	m_currentPocket = Main;
	
	IEntitySystem *entitySystem = gEnv->pEntitySystem;

	//not sure if I need a layer but if I use can set no physics to all objects inside.
	entitySystem->AddLayer("backpack","Main",//my guess for name of the parent
									   1<<3,//hopfully this bit is not used
									   false,//no physics needed for backpack									
									   -1,//eSpecType_All
									   false);//Default Loaded does not do anything?

	//Here we will create all the Entities 
	entitySystem->BeginCreateEntities(6);

	//Todo set the spawn params
	SEntitySpawnParams spawnParams;

	//create the entities out of the xml and hide them
	//entitySystem->CreateEntity()

	entitySystem->EndCreateEntities();

	//attach the backpack to character on a bone and the items to the backpack bones
	SChildAttachParams attachParams;
	attachParams.m_nAttachFlags |= BIT(2);// ATTACHMENT_CHARACTERBONE;

	isOpen = false;
}

void Backpack::EnterInventory()
{
	ToggleActionMaps();
	//ToDo add enter animation mannequin code
}

void Backpack::ExitInventory()
{
	ToggleActionMaps();
	//ToDo add exit animation mannequin code
}

void Backpack::goPocket(Pocket pkt)
{
	m_currentPocket = pkt;
	//ToDo add animation mannequin code
	switch (pkt)
	{
	case Main:
		break;
	case LeftSide:
		break;
	case RightSide:
		break;
	case Front:
		break;
	default:
		break;
	}

}

//ToDo check whether the next/prev item is there, material change of item
//and move the hand with mannequin
void Backpack::NextItem()
{
	
}

void Backpack::PrevItem()
{

}
//note: initializes on first item.
//note: wrap around will need to be implemented.

void Backpack::AddItemBP(Pocket pkt, Position pos)
{
	switch (pkt)
	{
	case Main:
		m_mainPocket |= pos;
		break;
	case LeftSide:
		m_leftPocket |= pos;
		break;
	case RightSide:
		m_rightPocket |= pos;
		break;
	case Front:
		m_frontPocket |= pos;
		break;
	default:
		break;
	}
	__super::AddItem(getItemId(pkt, pos));
}

void Backpack::RemoveItemBP(Pocket pkt, Position pos) 
{
	switch (pkt)
	{
	case Main:
		m_mainPocket &= ~pos;
		break;
	case LeftSide:
		m_leftPocket &= ~pos;
		break;
	case RightSide:
		m_rightPocket &= ~pos;
		break;
	case Front:
		m_frontPocket &= ~pos;
		break;
	default:
		break;
	}
	__super::RemoveItem(getItemId(pkt, pos));
}

void Backpack::RemoveAllItems(bool force)
{
	__super::RemoveAllItems(force);
}

bool Backpack::hasItem(Pocket pkt, Position pos)
{
	switch (pkt)
	{
	case Main:
		if (m_mainPocket & pos)
			return true;
		break;
	case LeftSide:
		if (m_leftPocket & pos)
			return true;
		break;
	case RightSide:
		if (m_rightPocket & pos)
			return true;
		break;
	case Front:
		if (m_frontPocket & pos)
			return true;
		break;
	default:
		return false;
		break;
	}
	return false;
}

EntityId Backpack::getItemId(Pocket pkt, Position pos)
{
	if (hasItem(pkt, pos))
		/*returnthe entityId with corresponding
		pocket and position*/;
	else
		return 0;
	return 0;
}

void Backpack::ToggleActionMaps()
{
	IActionMapManager* pActionMapMan = m_pGameFrameWork->GetIActionMapManager();
	CRY_ASSERT(pActionMapMan);
	pActionMapMan->EnableActionMap("player", isOpen);

	IActionMap* pActionMap = pActionMapMan->GetActionMap("backpack");
	EntityId player = m_pGameFrameWork->GetClientActorId();

	pActionMap->SetActionListener(player);
	pActionMapMan->EnableActionMap("backpack", !isOpen);
	isOpen = !isOpen;
}