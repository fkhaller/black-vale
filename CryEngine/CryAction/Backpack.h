#ifndef _BACKPACK_H_
#define _BACKPACK_H_

#include "Inventory.h"

enum Pocket
{
	Main      = 0,
	LeftSide  = 1,
	RightSide = 2,
	Front     = 3
};

enum Position
{
	Slot1  = 0x001,
	Slot2  = 0x002,
	Slot3  = 0x004,
	Slot4  = 0x008,
	Slot5  = 0x010,
	Slot6  = 0x020,
	Slot7  = 0x040,
	Slot8  = 0x080,
	Slot9  = 0x100,
	Slot10 = 0x200
};

class Backpack :
	public CInventory
{
public:
	//initialization of Entitys and storage of these
	//ids relating to all the items needed.
	Backpack();

	//TODO deconstructor of all entities
	//~Backpack();

	//switchess the action map and play
	//the opening backpack animation. defaults to
	//main pocket
	void EnterInventory();
	//switches actionmap back and animation
	//to go back
	void ExitInventory();

	//selection of pockets play the animation to
	//switch pockets. Initializes hovering hand.
	void goPocket(Pocket);

	//selection of items. controls the hovering hand
	//and the material change of items.
	void NextItem();
	void PrevItem();

	//adding/removing items call the add/remove functions
	//that manipulate the EntityIds
	void AddItemBP(Pocket, Position);
	void RemoveItemBP(Pocket, Position);
	virtual void RemoveAllItems(bool);

	//has the item?
	bool hasItem(Pocket, Position);

	//will return the EntityId of the item
	EntityId getItemId(Pocket, Position);

private:
	//switch action map
	void ToggleActionMaps();
	bool isOpen;
	
	//current pocket
	Pocket m_currentPocket;

	//flags for the items
	uint32 m_mainPocket;
	uint32 m_leftPocket;
	uint32 m_rightPocket;
	uint32 m_frontPocket;

	//items in backpack and the backpack itself
	CEntity *m_backpack;
	CEntity *m_compass; 
	CEntity *m_flashlight;
	CEntity *m_lighter;
	CEntity *m_knife;
	CEntity *m_map;
	CEntity *m_journal;
};

#endif