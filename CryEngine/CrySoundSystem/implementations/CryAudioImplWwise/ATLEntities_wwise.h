// Copyright 2001-2015 Crytek GmbH. All rights reserved.

#pragma once

#include <ATLEntityData.h>
#include <IAudioSystemImplementation.h>
#include <AK/SoundEngine/Common/AkTypes.h>

typedef std::vector<AkUniqueID, STLSoundAllocator<AkUniqueID> > TAKUniqueIDVector;

struct SATLAudioObjectData_wwise : public IATLAudioObjectData
{
	typedef std::map<AkAuxBusID, float, std::less<AkAuxBusID>, 
		STLSoundAllocator<std::pair<AkAuxBusID, float> > > TEnvironmentImplMap;

	explicit SATLAudioObjectData_wwise(AkGameObjectID const _AKID, bool const _bHasPosition)
		: nAKID(_AKID)
		, bHasPosition(_bHasPosition)
		, bNeedsToUpdateEnvironments(false)
	{}

	virtual ~SATLAudioObjectData_wwise() {}

	AkGameObjectID const nAKID;
	bool const           bHasPosition;
	bool                 bNeedsToUpdateEnvironments;
	TEnvironmentImplMap  cEnvironemntImplAmounts;

	DELETE_DEFAULT_CONSTRUCTOR(SATLAudioObjectData_wwise);
	PREVENT_OBJECT_COPY(SATLAudioObjectData_wwise);
};

struct SATLListenerData_wwise : public IATLListenerData
{
	explicit SATLListenerData_wwise(AkUniqueID const nPassedAKID)
		: nAKID(nPassedAKID)
	{}

	virtual ~SATLListenerData_wwise() {}

	AkUniqueID const nAKID;

	DELETE_DEFAULT_CONSTRUCTOR(SATLListenerData_wwise);
	PREVENT_OBJECT_COPY(SATLListenerData_wwise);
};

struct SATLTriggerImplData_wwise : public IATLTriggerImplData
{
	explicit SATLTriggerImplData_wwise(AkUniqueID const nPassedAKID)
		: nAKID(nPassedAKID)
	{}

	virtual ~SATLTriggerImplData_wwise() {}

	AkUniqueID const nAKID;

	DELETE_DEFAULT_CONSTRUCTOR(SATLTriggerImplData_wwise);
	PREVENT_OBJECT_COPY(SATLTriggerImplData_wwise);
};

struct SATLRtpcImplData_wwise : public IATLRtpcImplData
{
	explicit SATLRtpcImplData_wwise(AkRtpcID const nPassedAKID, float const m_fPassedMult, float const m_fPassedShift)
		: m_fMult(m_fPassedMult)
		, m_fShift(m_fPassedShift)
		, nAKID(nPassedAKID)
	{}

	virtual ~SATLRtpcImplData_wwise() {}

	float const m_fMult;
	float const m_fShift;
	AkRtpcID const nAKID;

	DELETE_DEFAULT_CONSTRUCTOR(SATLRtpcImplData_wwise);
	PREVENT_OBJECT_COPY(SATLRtpcImplData_wwise);
};

enum EWwiseSwitchType : TATLEnumFlagsType
{
	eWST_NONE   = 0,
	eWST_SWITCH = 1,
	eWST_STATE  = 2,
	eWST_RTPC   = 3,
};

struct SATLSwitchStateImplData_wwise : public IATLSwitchStateImplData
{
	explicit SATLSwitchStateImplData_wwise(
		EWwiseSwitchType const ePassedType,
		AkUInt32 const nPassedAKSwitchID,
		AkUInt32 const nPassedAKStateID,
		float const fPassedRtpcValue = 0.0f)
		: eType(ePassedType)
		, nAKSwitchlID(nPassedAKSwitchID)
		, nAKStateID(nPassedAKStateID)
		, fRtpcValue(fPassedRtpcValue)
	{}

	virtual ~SATLSwitchStateImplData_wwise() {}

	EWwiseSwitchType const eType;
	AkUInt32 const         nAKSwitchlID;
	AkUInt32 const         nAKStateID;
	float const            fRtpcValue;

	DELETE_DEFAULT_CONSTRUCTOR(SATLSwitchStateImplData_wwise);
	PREVENT_OBJECT_COPY(SATLSwitchStateImplData_wwise);
};

enum EWwiseAudioEnvironmentType : TATLEnumFlagsType
{
	eWAET_NONE    = 0,
	eWAET_AUX_BUS = 1,
	eWAET_RTPC    = 2,
};

struct SATLEnvironmentImplData_wwise : public IATLEnvironmentImplData
{
	explicit SATLEnvironmentImplData_wwise(EWwiseAudioEnvironmentType const ePassedType)
		: eType(ePassedType)
	{}

	explicit SATLEnvironmentImplData_wwise(EWwiseAudioEnvironmentType const ePassedType, AkAuxBusID const nPassedAKBusID)
		: eType(ePassedType)
		, nAKBusID(nPassedAKBusID)
	{
		CRY_ASSERT(ePassedType == eWAET_AUX_BUS);
	}

	explicit SATLEnvironmentImplData_wwise(
		EWwiseAudioEnvironmentType const ePassedType,
		AkRtpcID const nPassedAKRtpcID,
		float const fPassedMult,
		float const fPassedShift)
		: eType(ePassedType)
		, nAKRtpcID(nPassedAKRtpcID)
		, fMult(fPassedMult)
		, fShift(fPassedShift)
	{
		CRY_ASSERT(ePassedType == eWAET_RTPC);
	}

	virtual ~SATLEnvironmentImplData_wwise() {}

	EWwiseAudioEnvironmentType const eType;

	union
	{
		// Aux Bus implementation
		struct
		{
			AkAuxBusID nAKBusID;
		};

		// Rtpc implementation
		struct
		{
			AkRtpcID nAKRtpcID;
			float    fMult;
			float    fShift;
		};
	};

	DELETE_DEFAULT_CONSTRUCTOR(SATLEnvironmentImplData_wwise);
	PREVENT_OBJECT_COPY(SATLEnvironmentImplData_wwise);
};

struct SATLEventData_wwise : public IATLEventData
{
	explicit SATLEventData_wwise(TAudioEventID const nPassedID)
		: audioEventState(eAudioEventState_None)
		, nAKID(AK_INVALID_UNIQUE_ID)
		, nATLID(nPassedID)
	{}

	virtual ~SATLEventData_wwise() {}

	EAudioEventState    audioEventState;
	AkUniqueID          nAKID;
	TAudioEventID const nATLID;

	DELETE_DEFAULT_CONSTRUCTOR(SATLEventData_wwise);
	PREVENT_OBJECT_COPY(SATLEventData_wwise);
};

struct SATLAudioFileEntryData_wwise : public IATLAudioFileEntryData
{
	SATLAudioFileEntryData_wwise()
		: nAKBankID(AK_INVALID_BANK_ID)
	{}

	virtual ~SATLAudioFileEntryData_wwise() {}

	AkBankID nAKBankID;

	PREVENT_OBJECT_COPY(SATLAudioFileEntryData_wwise);
};

struct SATLAudioStandaloneFileData_wwise : public IATLAudioStandaloneFileData
{
};
