// Copyright 2001-2015 Crytek GmbH. All rights reserved.

#pragma once

#include <IAudioSystemImplementation.h>
#include <STLSoundAllocator.h>

namespace SDLMixer
{
	typedef uint TSampleID;
	typedef uint TEventID;
	typedef uint TListenerID;
}

typedef std::set<int, std::less<int>, STLSoundAllocator<int> > TChannelSet;

struct SATLTriggerImplData_sdlmixer : public IATLTriggerImplData
{
	explicit SATLTriggerImplData_sdlmixer(const SDLMixer::TEventID nID)
		: nSDLMixerEventID(nID)
		, nSampleID(0)
		, fAttenuationMinDistance(0)
		, fAttenuationMaxDistance(100)
		, nVolume(128)
		, nLoopCount(1)
		, bPanningEnabled(true)
		, bStartEvent(true)
	{}

	const SDLMixer::TEventID nSDLMixerEventID;
	SDLMixer::TSampleID nSampleID;
	float fAttenuationMinDistance;
	float fAttenuationMaxDistance;
	int nVolume;
	int nLoopCount;
	bool bPanningEnabled;
	bool bStartEvent;
};

struct SATLRtpcImplData_sdlmixer : public IATLRtpcImplData
{
	// Empty implementation so that the engine has something
	// to refer to since RTPCs are not currently supported by
	// the SDL Mixer implementation
};

struct SATLSwitchStateImplData_sdlmixer : public IATLSwitchStateImplData
{
	// Empty implementation so that the engine has something
	// to refer to since switches are not currently supported by
	// the SDL Mixer implementation
};

struct SATLEnvironmentImplData_sdlmixer : public IATLEnvironmentImplData
{
	// Empty implementation so that the engine has something
	// to refer to since environments are not currently supported by
	// the SDL Mixer implementation
};

struct SATLEventData_sdlmixer : public IATLEventData
{
	explicit SATLEventData_sdlmixer(TAudioEventID const nPassedID)
		: nEventID(nPassedID)
		, pStaticData(nullptr)
	{}

	void Reset()
	{
		channels.clear();
		pStaticData = nullptr;
	}
	const TAudioEventID nEventID;
	TChannelSet channels;
	const SATLTriggerImplData_sdlmixer* pStaticData;
};
typedef std::set<SATLEventData_sdlmixer*, std::less<SATLEventData_sdlmixer*>, STLSoundAllocator<SATLEventData_sdlmixer*> > TEventInstanceSet;

struct SATLAudioObjectData_sdlmixer : public IATLAudioObjectData
{
	SATLAudioObjectData_sdlmixer(TAudioObjectID nID, bool bIsGlobal)
		: nObjectID(nID)
		, bGlobal(bIsGlobal)
		, bPositionChanged(false) {}

	const TAudioObjectID nObjectID;
	CAudioObjectTransformation position;
	TEventInstanceSet events;
	bool bGlobal;
	bool bPositionChanged;
};

struct SATLListenerData_sdlmixer : public IATLListenerData
{
	explicit SATLListenerData_sdlmixer(const SDLMixer::TListenerID nID)
		: nListenerID(nID)
	{}

	const SDLMixer::TListenerID nListenerID;
};

struct SATLAudioFileEntryData_sdlmixer : public IATLAudioFileEntryData
{
	SATLAudioFileEntryData_sdlmixer()
	{}

	SDLMixer::TSampleID nSampleID;
};

struct SATLAudioStandaloneFile_sdlmixer : public IATLAudioStandaloneFileData
{
	// Empty implementation so that the engine has something
	// to refer to since support for standalone audio files
	// is currently not implemented to the SDL Mixer implementation
};
