// Copyright 2001-2015 Crytek GmbH. All rights reserved.

#pragma once

class CAudioSystemImplCVars
{
public:

	CAudioSystemImplCVars();
	~CAudioSystemImplCVars();

	void RegisterVariables();
	void UnregisterVariables();

	int  m_nPrimaryPoolSize;

	PREVENT_OBJECT_COPY(CAudioSystemImplCVars);
};

extern CAudioSystemImplCVars g_SDLMixerImplCVars;
