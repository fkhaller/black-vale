// Copyright 2001-2015 Crytek GmbH. All rights reserved.

#include "stdafx.h"
#include "AudioSystemImpl_sdlmixer.h"
#include "AudioSystemImplCVars_sdlmixer.h"
#include <CryFile.h>
#include <CryPath.h>
#include "SDLMixerSoundEngineUtil.h"
#include <IAudioSystem.h>
#include "SDLMixerSoundEngineTypes.h"

// SDL / SDL Mixer
#include <SDL.h>
#include <SDL_rwops.h>
#include <SDL_mixer.h>

char const* const CAudioSystemImpl_sdlmixer::s_szSDLFileTag = "SDLMixerSample";
char const* const CAudioSystemImpl_sdlmixer::s_szSDLCommonAttribute = "sdl_name";
char const* const CAudioSystemImpl_sdlmixer::s_szSDLEventIdTag = "event_id";
char const* const CAudioSystemImpl_sdlmixer::s_szSDLEventTag = "SDLMixerEvent";
char const* const CAudioSystemImpl_sdlmixer::s_szSDLSoundLibraryPath = "/sounds/sdlmixer/";
char const* const CAudioSystemImpl_sdlmixer::s_szSDLEventTypeTag = "event_type";
char const* const CAudioSystemImpl_sdlmixer::s_szSDLEventPanningEnabledTag = "enable_panning";
char const* const CAudioSystemImpl_sdlmixer::s_szSDLEventAttenuationEnabledTag = "enable_distance_attenuation";
char const* const CAudioSystemImpl_sdlmixer::s_szSDLEventAttenuationMinDistanceTag = "attenuation_dist_min";
char const* const CAudioSystemImpl_sdlmixer::s_szSDLEventAttenuationMaxDistanceTag = "attenuation_dist_max";
char const* const CAudioSystemImpl_sdlmixer::s_szSDLEventVolumeTag = "volume";
char const* const CAudioSystemImpl_sdlmixer::s_szSDLEventLoopCountTag = "loop_count";

void EndEventCallback(TAudioEventID nEventID)
{
	SAudioRequest oRequest;
	SAudioCallbackManagerRequestData<eACMRT_REPORT_FINISHED_EVENT> oRequestData(nEventID, true);
	oRequest.nFlags = eARF_THREAD_SAFE_PUSH;
	oRequest.pData = &oRequestData;
	gEnv->pAudioSystem->PushRequest(oRequest);
}

CAudioSystemImpl_sdlmixer::CAudioSystemImpl_sdlmixer()
{
	m_sGameFolder = PathUtil::GetGameFolder();
	if (m_sGameFolder.empty())
	{
		CryFatalError("<Audio - SDLMixer>: Needs a valid game folder to proceed!");
	}

#if defined(INCLUDE_SDLMIXER_IMPL_PRODUCTION_CODE)
	m_sFullImplString = "SDL Mixer 2.0 (";
	m_sFullImplString += m_sGameFolder + PathUtil::RemoveSlash(s_szSDLSoundLibraryPath) + ")";
#endif // INCLUDE_SDLMIXER_IMPL_PRODUCTION_CODE

#if CRY_PLATFORM_WINDOWS
	m_nMemoryAlignment = 16;
#elif CRY_PLATFORM_MAC
	m_nMemoryAlignment = 16;
#elif CRY_PLATFORM_LINUX
	m_nMemoryAlignment = 16;
#elif CRY_PLATFORM_IOS
	m_nMemoryAlignment = 16;
#elif CRY_PLATFORM_ANDROID
	m_nMemoryAlignment = 16;
#else
#error "Undefined platform."
#endif
}

CAudioSystemImpl_sdlmixer::~CAudioSystemImpl_sdlmixer()
{
}

void CAudioSystemImpl_sdlmixer::Update(float const fUpdateIntervalMS)
{
	SDLMixer::SoundEngine::Update();
}

EAudioRequestStatus CAudioSystemImpl_sdlmixer::Init()
{
	if (SDLMixer::SoundEngine::Init())
	{
		SDLMixer::SoundEngine::RegisterEventFinishedCallback(EndEventCallback);
		return eARS_SUCCESS;
	}

	return eARS_FAILURE;
}

EAudioRequestStatus CAudioSystemImpl_sdlmixer::ShutDown()
{
	return eARS_SUCCESS;
}

EAudioRequestStatus CAudioSystemImpl_sdlmixer::Release()
{
	SDLMixer::SoundEngine::Release();
	POOL_FREE(this);

	// Freeing Memory Pool Memory again
	uint8* pMemSystem = g_SDLMixerImplMemoryPool.Data();
	g_SDLMixerImplMemoryPool.UnInitMem();

	if (pMemSystem)
	{
		delete[](uint8*)(pMemSystem);
	}

	g_SDLMixerImplCVars.UnregisterVariables();

	return eARS_SUCCESS;
}

void CAudioSystemImpl_sdlmixer::OnAudioSystemRefresh()
{
	SDLMixer::SoundEngine::Refresh();
}

EAudioRequestStatus CAudioSystemImpl_sdlmixer::OnLoseFocus()
{
	SDLMixer::SoundEngine::Pause();
	return eARS_SUCCESS;
}

EAudioRequestStatus CAudioSystemImpl_sdlmixer::OnGetFocus()
{
	SDLMixer::SoundEngine::Resume();
	return eARS_SUCCESS;
}

EAudioRequestStatus CAudioSystemImpl_sdlmixer::MuteAll()
{
	SDLMixer::SoundEngine::Mute();
	return eARS_SUCCESS;
}

EAudioRequestStatus CAudioSystemImpl_sdlmixer::UnmuteAll()
{
	SDLMixer::SoundEngine::UnMute();
	return eARS_SUCCESS;
}

EAudioRequestStatus CAudioSystemImpl_sdlmixer::StopAllSounds()
{
	SDLMixer::SoundEngine::Stop();
	return eARS_SUCCESS;
}

EAudioRequestStatus CAudioSystemImpl_sdlmixer::RegisterAudioObject(IATLAudioObjectData* const pObjectData, char const* const sObjectName)
{
	SATLAudioObjectData_sdlmixer* const pSDLMixerObject = static_cast<SATLAudioObjectData_sdlmixer* const>(pObjectData);

	if (pSDLMixerObject)
	{
		SDLMixer::SoundEngine::RegisterAudioObject(pSDLMixerObject);
#if defined(INCLUDE_SDLMIXER_IMPL_PRODUCTION_CODE)
		m_idToName[pSDLMixerObject->nObjectID] = sObjectName;
#endif
		return eARS_SUCCESS;
	}
	return eARS_FAILURE;
}

EAudioRequestStatus CAudioSystemImpl_sdlmixer::RegisterAudioObject(IATLAudioObjectData* const pObjectData)
{
	SATLAudioObjectData_sdlmixer* const pSDLMixerObject = static_cast<SATLAudioObjectData_sdlmixer* const>(pObjectData);
	if (pSDLMixerObject)
	{
		SDLMixer::SoundEngine::RegisterAudioObject(pSDLMixerObject);
		return eARS_SUCCESS;
	}
	return eARS_FAILURE;
}

EAudioRequestStatus CAudioSystemImpl_sdlmixer::UnregisterAudioObject(IATLAudioObjectData* const pObjectData)
{
	SATLAudioObjectData_sdlmixer* const pSDLMixerObject = static_cast<SATLAudioObjectData_sdlmixer* const>(pObjectData);
	if (pSDLMixerObject)
	{
		SDLMixer::SoundEngine::UnregisterAudioObject(pSDLMixerObject);
		return eARS_SUCCESS;
	}
	return eARS_FAILURE;
}

EAudioRequestStatus CAudioSystemImpl_sdlmixer::ResetAudioObject(IATLAudioObjectData* const pObjectData)
{
	return eARS_SUCCESS;
}

EAudioRequestStatus CAudioSystemImpl_sdlmixer::UpdateAudioObject(IATLAudioObjectData* const pObjectData)
{
	return eARS_SUCCESS;
}

EAudioRequestStatus CAudioSystemImpl_sdlmixer::PlayFile(SATLAudioStandaloneFileInfo* const _pAudioStandaloneFileInfo)
{
	return eARS_SUCCESS;
}

EAudioRequestStatus CAudioSystemImpl_sdlmixer::StopFile(SATLAudioStandaloneFileInfo* const _pAudioStandaloneFileInfo)
{
	return eARS_SUCCESS;
}

EAudioRequestStatus CAudioSystemImpl_sdlmixer::PrepareTriggerSync(IATLAudioObjectData* const pAudioObjectData, IATLTriggerImplData const* const pTriggerData)
{
	return eARS_SUCCESS;
}

EAudioRequestStatus CAudioSystemImpl_sdlmixer::UnprepareTriggerSync(IATLAudioObjectData* const pAudioObjectData, IATLTriggerImplData const* const pTriggerData)
{
	return eARS_SUCCESS;
}

EAudioRequestStatus CAudioSystemImpl_sdlmixer::PrepareTriggerAsync(IATLAudioObjectData* const pAudioObjectData, IATLTriggerImplData const* const pTriggerData, IATLEventData* const pEventData)
{
	return eARS_SUCCESS;
}

EAudioRequestStatus CAudioSystemImpl_sdlmixer::UnprepareTriggerAsync(IATLAudioObjectData* const pAudioObjectData, IATLTriggerImplData const* const pTriggerData, IATLEventData* const pEventData)
{
	return eARS_SUCCESS;
}

EAudioRequestStatus CAudioSystemImpl_sdlmixer::ActivateTrigger(IATLAudioObjectData* const pAudioObjectData, IATLTriggerImplData const* const pTriggerData, IATLEventData* const pEventData)
{
	EAudioRequestStatus eResult = eARS_FAILURE;

	SATLAudioObjectData_sdlmixer* const pSDLAudioObjectData = static_cast<SATLAudioObjectData_sdlmixer* const>(pAudioObjectData);
	SATLTriggerImplData_sdlmixer const* const pSDLEventStaticData = static_cast<SATLTriggerImplData_sdlmixer const* const>(pTriggerData);
	SATLEventData_sdlmixer* const pSDLEventInstanceData = static_cast<SATLEventData_sdlmixer* const>(pEventData);

	if ((pSDLAudioObjectData != nullptr) && (pSDLEventStaticData != nullptr) && (pSDLEventInstanceData != nullptr))
	{
		if (SDLMixer::SoundEngine::ExecuteEvent(pSDLAudioObjectData, pSDLEventStaticData, pSDLEventInstanceData))
		{
			eResult = eARS_SUCCESS;
		}
	}
	return eResult;
}

EAudioRequestStatus CAudioSystemImpl_sdlmixer::StopEvent(IATLAudioObjectData* const pAudioObjectData, IATLEventData const* const pEventData)
{
	EAudioRequestStatus eResult = eARS_FAILURE;
	SATLEventData_sdlmixer const* const pSDLEventInstanceData = static_cast<SATLEventData_sdlmixer const* const>(pEventData);

	if (pSDLEventInstanceData != nullptr)
	{
		if (SDLMixer::SoundEngine::StopEvent(pSDLEventInstanceData))
		{
			eResult = eARS_SUCCESS;
		}
	}
	return eResult;
}

EAudioRequestStatus CAudioSystemImpl_sdlmixer::StopAllEvents(IATLAudioObjectData* const pAudioObjectData)
{
	return eARS_SUCCESS;
}

EAudioRequestStatus CAudioSystemImpl_sdlmixer::SetPosition(IATLAudioObjectData* const pAudioObjectData, CAudioObjectTransformation const& oWorldPosition)
{
	SATLAudioObjectData_sdlmixer* const pSDLMixerObject = static_cast<SATLAudioObjectData_sdlmixer* const>(pAudioObjectData);
	if (pSDLMixerObject)
	{
		SDLMixer::SoundEngine::SetAudioObjectPosition(pSDLMixerObject, oWorldPosition);
		return eARS_SUCCESS;
	}
	return eARS_FAILURE;
}

EAudioRequestStatus CAudioSystemImpl_sdlmixer::SetRtpc(IATLAudioObjectData* const pAudioObjectData, IATLRtpcImplData const* const pRtpcData, float const fValue)
{
	return eARS_SUCCESS;
}

EAudioRequestStatus CAudioSystemImpl_sdlmixer::SetSwitchState(IATLAudioObjectData* const pAudioObjectData, IATLSwitchStateImplData const* const pSwitchStateData)
{
	return eARS_SUCCESS;
}

EAudioRequestStatus CAudioSystemImpl_sdlmixer::SetObstructionOcclusion(IATLAudioObjectData* const pAudioObjectData, float const fObstruction, float const fOcclusion)
{
	return eARS_SUCCESS;
}

EAudioRequestStatus CAudioSystemImpl_sdlmixer::SetEnvironment(IATLAudioObjectData* const pAudioObjectData, IATLEnvironmentImplData const* const pEnvironmentImplData, float const fAmount)
{
	return eARS_SUCCESS;
}

EAudioRequestStatus CAudioSystemImpl_sdlmixer::SetListenerPosition(IATLListenerData* const pListenerData, CAudioObjectTransformation const& oNewPosition)
{
	SATLListenerData_sdlmixer* const pListener = static_cast<SATLListenerData_sdlmixer* const>(pListenerData);
	if (pListener)
	{
		SDLMixer::SoundEngine::SetListenerPosition(pListener->nListenerID, oNewPosition);
		return eARS_SUCCESS;
	}
	return eARS_FAILURE;
}

EAudioRequestStatus CAudioSystemImpl_sdlmixer::RegisterInMemoryFile(SATLAudioFileEntryInfo* const pAudioFileEntry)
{
	EAudioRequestStatus eResult = eARS_FAILURE;

	if (pAudioFileEntry != nullptr)
	{
		SATLAudioFileEntryData_sdlmixer* const pFileData = static_cast<SATLAudioFileEntryData_sdlmixer*>(pAudioFileEntry->pImplData);

		if (pFileData != nullptr)
		{
			pFileData->nSampleID = SDLMixer::SoundEngine::LoadSample(pAudioFileEntry->pFileData, pAudioFileEntry->nSize, pAudioFileEntry->sFileName);
			eResult = eARS_SUCCESS;
		}
		else
		{
			g_SDLMixerImplLogger.Log(eALT_ERROR, "Invalid AudioFileEntryData passed to the SDL Mixer implementation of RegisterInMemoryFile");
		}
	}

	return eResult;
}

EAudioRequestStatus CAudioSystemImpl_sdlmixer::UnregisterInMemoryFile(SATLAudioFileEntryInfo* const pAudioFileEntry)
{
	EAudioRequestStatus eResult = eARS_FAILURE;

	if (pAudioFileEntry != nullptr)
	{
		SATLAudioFileEntryData_sdlmixer* const pFileData = static_cast<SATLAudioFileEntryData_sdlmixer*>(pAudioFileEntry->pImplData);

		if (pFileData != nullptr)
		{
			SDLMixer::SoundEngine::UnloadSample(pFileData->nSampleID);
			eResult = eARS_SUCCESS;
		}
		else
		{
			g_SDLMixerImplLogger.Log(eALT_ERROR, "Invalid AudioFileEntryData passed to the SDL Mixer implementation of UnregisterInMemoryFile");
		}
	}

	return eResult;
}

EAudioRequestStatus CAudioSystemImpl_sdlmixer::ParseAudioFileEntry(XmlNodeRef const pAudioFileEntryNode, SATLAudioFileEntryInfo* const pFileEntryInfo)
{
	EAudioRequestStatus eResult = eARS_FAILURE;

	if ((_stricmp(pAudioFileEntryNode->getTag(), s_szSDLFileTag) == 0) && (pFileEntryInfo != nullptr))
	{
		char const* const sFileName = pAudioFileEntryNode->getAttr(s_szSDLCommonAttribute);

		// Currently the SDLMixer Implementation does not support localized files.
		pFileEntryInfo->bLocalized = false;

		if (sFileName != nullptr && sFileName[0] != '\0')
		{
			pFileEntryInfo->sFileName = sFileName;
			pFileEntryInfo->nMemoryBlockAlignment	= m_nMemoryAlignment;
			POOL_NEW(SATLAudioFileEntryData_sdlmixer, pFileEntryInfo->pImplData);

			eResult = eARS_SUCCESS;
		}
		else
		{
			pFileEntryInfo->sFileName = nullptr;
			pFileEntryInfo->nMemoryBlockAlignment	= 0;
			pFileEntryInfo->pImplData = nullptr;
		}
	}

	return eResult;
}

void CAudioSystemImpl_sdlmixer::DeleteAudioFileEntryData(IATLAudioFileEntryData* const pOldAudioFileEntryData)
{
	POOL_FREE(pOldAudioFileEntryData);
}

char const* const CAudioSystemImpl_sdlmixer::GetAudioFileLocation(SATLAudioFileEntryInfo* const pFileEntryInfo)
{
	static CryFixedStringT<MAX_AUDIO_FILE_PATH_LENGTH> sPath;
	sPath = m_sGameFolder.c_str();
	sPath += s_szSDLSoundLibraryPath;

	return sPath.c_str();
}

IATLTriggerImplData const* CAudioSystemImpl_sdlmixer::NewAudioTriggerImplData(XmlNodeRef const pAudioTriggerNode)
{
	SATLTriggerImplData_sdlmixer* pNewTriggerImpl = nullptr;
	if (_stricmp(pAudioTriggerNode->getTag(), s_szSDLEventTag) == 0)
	{
		char const* const sFileName = pAudioTriggerNode->getAttr(s_szSDLCommonAttribute);
		SDLMixer::TSampleID sampleID = SDLMixer::GetIDFromString(sFileName);

		SDLMixer::TEventID eventID = 0;
		pAudioTriggerNode->getAttr(s_szSDLEventIdTag, eventID);

		pNewTriggerImpl = SDLMixer::SoundEngine::CreateEventData(eventID);
		if (pNewTriggerImpl)
		{
			pNewTriggerImpl->nSampleID = sampleID;

			pNewTriggerImpl->bStartEvent = (_stricmp(pAudioTriggerNode->getAttr(s_szSDLEventTypeTag), "stop") != 0);
			if (pNewTriggerImpl->bStartEvent)
			{
				pNewTriggerImpl->bPanningEnabled = (_stricmp(pAudioTriggerNode->getAttr(s_szSDLEventPanningEnabledTag), "true") == 0);
				bool bAttenuationEnabled = (_stricmp(pAudioTriggerNode->getAttr(s_szSDLEventAttenuationEnabledTag), "true") == 0);
				if (bAttenuationEnabled)
				{
					pAudioTriggerNode->getAttr(s_szSDLEventAttenuationMinDistanceTag, pNewTriggerImpl->fAttenuationMinDistance);
					pAudioTriggerNode->getAttr(s_szSDLEventAttenuationMaxDistanceTag, pNewTriggerImpl->fAttenuationMaxDistance);
				}
				else
				{
					pNewTriggerImpl->fAttenuationMinDistance = -1.0f;
					pNewTriggerImpl->fAttenuationMaxDistance = -1.0f;
				}

				// Translate decibel to normalized value.
				static const int nMaxVolume = 128;
				float fVolume = 0.0f;
				pAudioTriggerNode->getAttr(s_szSDLEventVolumeTag, fVolume);
				pNewTriggerImpl->nVolume = static_cast<int>(pow_tpl(10.0f, fVolume / 20.0f) * nMaxVolume);

				pAudioTriggerNode->getAttr(s_szSDLEventLoopCountTag, pNewTriggerImpl->nLoopCount);
			}
		}
	}

	return pNewTriggerImpl;
}

void CAudioSystemImpl_sdlmixer::DeleteAudioTriggerImplData(IATLTriggerImplData const* const pOldTriggerImplData)
{
	SATLTriggerImplData_sdlmixer const* const pSDLEventStaticData = static_cast<SATLTriggerImplData_sdlmixer const* const>(pOldTriggerImplData);
	if (pSDLEventStaticData)
	{
		SDLMixer::SoundEngine::StopEvents(pSDLEventStaticData->nSDLMixerEventID);
	}
	POOL_FREE_CONST(pOldTriggerImplData);
}

IATLRtpcImplData const* CAudioSystemImpl_sdlmixer::NewAudioRtpcImplData(XmlNodeRef const pAudioRtpcNode)
{
	SATLRtpcImplData_sdlmixer* pNewRtpcImpl = nullptr;
	POOL_NEW(SATLRtpcImplData_sdlmixer, pNewRtpcImpl)();
	return pNewRtpcImpl;
}

void CAudioSystemImpl_sdlmixer::DeleteAudioRtpcImplData(IATLRtpcImplData const* const pOldRtpcImplData)
{
	POOL_FREE_CONST(pOldRtpcImplData);
}

IATLSwitchStateImplData const* CAudioSystemImpl_sdlmixer::NewAudioSwitchStateImplData(XmlNodeRef const pAudioSwitchStateImplNode)
{
	SATLSwitchStateImplData_sdlmixer* pNewSwitchImpl = nullptr;
	POOL_NEW(SATLSwitchStateImplData_sdlmixer, pNewSwitchImpl)();
	return pNewSwitchImpl;
}

void CAudioSystemImpl_sdlmixer::DeleteAudioSwitchStateImplData(IATLSwitchStateImplData const* const pOldAudioSwitchStateImplNode)
{
	POOL_FREE_CONST(pOldAudioSwitchStateImplNode);
}

IATLEnvironmentImplData const* CAudioSystemImpl_sdlmixer::NewAudioEnvironmentImplData(XmlNodeRef const pAudioEnvironmentNode)
{
	SATLEnvironmentImplData_sdlmixer* pNewEnvironmentImpl = nullptr;
	POOL_NEW(SATLEnvironmentImplData_sdlmixer, pNewEnvironmentImpl)();
	return pNewEnvironmentImpl;
}

void CAudioSystemImpl_sdlmixer::DeleteAudioEnvironmentImplData(IATLEnvironmentImplData const* const pOldEnvironmentImplData)
{
	POOL_FREE_CONST(pOldEnvironmentImplData);
}

IATLAudioObjectData* CAudioSystemImpl_sdlmixer::NewGlobalAudioObjectData(TAudioObjectID const nObjectID)
{
	POOL_NEW_CREATE(SATLAudioObjectData_sdlmixer, pNewObject)(nObjectID, true);
	return pNewObject;
}

IATLAudioObjectData* CAudioSystemImpl_sdlmixer::NewAudioObjectData(TAudioObjectID const nObjectID)
{
	POOL_NEW_CREATE(SATLAudioObjectData_sdlmixer, pNewObject)(nObjectID, false);
	return pNewObject;
}

void CAudioSystemImpl_sdlmixer::DeleteAudioObjectData(IATLAudioObjectData* const pOldObjectData)
{
	POOL_FREE(pOldObjectData);
}

IATLListenerData* CAudioSystemImpl_sdlmixer::NewDefaultAudioListenerObjectData()
{
	POOL_NEW_CREATE(SATLListenerData_sdlmixer, pNewObject)(0);
	return pNewObject;
}

IATLListenerData* CAudioSystemImpl_sdlmixer::NewAudioListenerObjectData(uint const nIndex)
{
	POOL_NEW_CREATE(SATLListenerData_sdlmixer, pNewObject)(nIndex);
	return pNewObject;
}

void CAudioSystemImpl_sdlmixer::DeleteAudioListenerObjectData(IATLListenerData* const pOldListenerData)
{
	POOL_FREE(pOldListenerData);
}

IATLEventData* CAudioSystemImpl_sdlmixer::NewAudioEventData(TAudioEventID const nEventID)
{
	POOL_NEW_CREATE(SATLEventData_sdlmixer, pNewEvent)(nEventID);
	return pNewEvent;
}

void CAudioSystemImpl_sdlmixer::DeleteAudioEventData(IATLEventData* const pOldEventData)
{
	POOL_FREE(pOldEventData);
}

void CAudioSystemImpl_sdlmixer::ResetAudioEventData(IATLEventData* const pEventData)
{
	SATLEventData_sdlmixer* const pEventInstanceData = static_cast<SATLEventData_sdlmixer*>(pEventData);

	if (pEventInstanceData != nullptr)
	{
		pEventInstanceData->Reset();
	}
}

IATLAudioStandaloneFileData* CAudioSystemImpl_sdlmixer::NewAudioStandaloneFileData()
{
	POOL_NEW_CREATE(SATLAudioStandaloneFile_sdlmixer, pAudioStandaloneFile);
	return pAudioStandaloneFile;
}

void CAudioSystemImpl_sdlmixer::DeleteAudioStandaloneFileData(IATLAudioStandaloneFileData const* const _pOldAudioStandaloneFileData)
{
	POOL_FREE_CONST(_pOldAudioStandaloneFileData);
}

void CAudioSystemImpl_sdlmixer::ResetAudioStandaloneFileData(IATLAudioStandaloneFileData* const _pAudioStandaloneFileData)
{
}

void CAudioSystemImpl_sdlmixer::GamepadConnected(TAudioGamepadUniqueID const deviceUniqueID)
{
}

void CAudioSystemImpl_sdlmixer::GamepadDisconnected(TAudioGamepadUniqueID const deviceUniqueID)
{
}

void CAudioSystemImpl_sdlmixer::SetLanguage(char const* const sLanguage)
{
	//TODO
}

char const* const CAudioSystemImpl_sdlmixer::GetImplementationNameString() const
{
#if defined(INCLUDE_SDLMIXER_IMPL_PRODUCTION_CODE)
	return m_sFullImplString.c_str();
#endif // INCLUDE_SDLMIXER_IMPL_PRODUCTION_CODE
	return nullptr;
}

void CAudioSystemImpl_sdlmixer::GetMemoryInfo(SAudioImplMemoryInfo& oMemoryInfo) const
{
	oMemoryInfo.nPrimaryPoolSize          = g_SDLMixerImplMemoryPool.MemSize();
	oMemoryInfo.nPrimaryPoolUsedSize      = oMemoryInfo.nPrimaryPoolSize - g_SDLMixerImplMemoryPool.MemFree();
	oMemoryInfo.nPrimaryPoolAllocations   = g_SDLMixerImplMemoryPool.FragmentCount();
	oMemoryInfo.nBucketUsedSize           = g_SDLMixerImplMemoryPool.GetSmallAllocsSize();
	oMemoryInfo.nBucketAllocations        = g_SDLMixerImplMemoryPool.GetSmallAllocsCount();
	oMemoryInfo.nSecondaryPoolSize        = 0;
	oMemoryInfo.nSecondaryPoolUsedSize    = 0;
	oMemoryInfo.nSecondaryPoolAllocations = 0;
}
