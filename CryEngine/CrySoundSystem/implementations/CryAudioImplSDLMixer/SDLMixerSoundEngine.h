// Copyright 2001-2015 Crytek GmbH. All rights reserved.

#pragma once

#include "SDLMixerSoundEngineTypes.h"

namespace SDLMixer
{
	namespace SoundEngine
	{
		typedef void (*TFnEventCallback)(TAudioEventID);

		// Global events
		bool Init();
		void Release();
		void Refresh();
		void Update();

		void Pause();
		void Resume();

		void Mute();
		void UnMute();
		void Stop();

		// Load / Unload samples
		const TSampleID LoadSample(const string& sSampleFilePath);
		const TSampleID LoadSample(void* pMemory, const size_t nSize, const string& sSamplePath);
		void UnloadSample(const TSampleID nID);

		// Events
		SATLTriggerImplData_sdlmixer* CreateEventData(const TEventID nEventID);
		bool ExecuteEvent(SATLAudioObjectData_sdlmixer* const pAudioObject, SATLTriggerImplData_sdlmixer const* const pEventStaticData, SATLEventData_sdlmixer* const pEventInstance);
		// stops an specific event instance
		bool StopEvent(SATLEventData_sdlmixer const* const pEventInstance);
		// stops all the events of this type
		bool StopEvents(const SDLMixer::TEventID nEventID);

		// Listeners
		bool SetListenerPosition(const TEventID nListenerID, const CAudioObjectTransformation& position);

		// Audio Objects
		bool RegisterAudioObject(SATLAudioObjectData_sdlmixer* pAudioObjectData);
		bool UnregisterAudioObject(SATLAudioObjectData_sdlmixer* pAudioObjectData);
		bool SetAudioObjectPosition(SATLAudioObjectData_sdlmixer* pAudioObjectData, const CAudioObjectTransformation& position);

		// Callbacks
		void RegisterEventFinishedCallback(TFnEventCallback pCallbackFunction);
	}
}
