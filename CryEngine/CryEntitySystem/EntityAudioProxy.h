// Copyright 2001-2015 Crytek GmbH. All rights reserved.

#pragma once

#include <IAudioSystem.h>
#include <STLPoolAllocator.h>

//////////////////////////////////////////////////////////////////////////
// Description:
//    Handles audio on the entity.
//////////////////////////////////////////////////////////////////////////
struct CEntityAudioProxy : public IEntityAudioProxy
{
public:

	CEntityAudioProxy();
	virtual ~CEntityAudioProxy();

	// IComponent
	virtual void ProcessEvent(SEntityEvent& event);
	virtual void Initialize(SComponentInitializer const& init);
	// ~IComponent

	// IEntityProxy
	virtual EEntityProxy GetType() {return ENTITY_PROXY_AUDIO;}
	virtual void Release();
	virtual void Done();
	virtual void Update(SEntityUpdateContext& ctx) {}
	virtual bool Init(IEntity* pEntity, SEntitySpawnParams& params ) {return true;}
	virtual void Reload(IEntity* pEntity, SEntitySpawnParams& params);
	virtual void SerializeXML(XmlNodeRef &entityNode,bool bLoading) {}
	virtual void Serialize(TSerialize ser);
	virtual bool NeedSerialize() {return false;}
	virtual bool GetSignature(TSerialize signature);
	virtual void GetMemoryUsage(ICrySizer* pSizer) const {pSizer->AddObject(this, sizeof(*this));}
	// ~IEntityProxy

	// IEntityAudioProxy
	virtual void  SetFadeDistance(float const fFadeDistance) {m_fadeDistance = fFadeDistance;}
	virtual float GetFadeDistance() const {return m_fadeDistance;}
	virtual void  SetEnvironmentFadeDistance(float const fEnvironmentFadeDistance) {m_environmentFadeDistance = fEnvironmentFadeDistance;}
	virtual float GetEnvironmentFadeDistance() const {return m_environmentFadeDistance;}
	virtual void  SetEnvironmentID(TAudioEnvironmentID const nEnvironmentID) {m_audioEnvironmentID = nEnvironmentID;}
	virtual TAudioEnvironmentID GetEnvironmentID() const {return m_audioEnvironmentID;}
	virtual TAudioProxyID CreateAuxAudioProxy();
	virtual bool RemoveAuxAudioProxy(TAudioProxyID const nAudioProxyLocalID);
	virtual void SetAuxAudioProxyOffset(Matrix34 const& rOffset, TAudioProxyID const nAudioProxyLocalID = DEFAULT_AUDIO_PROXY_ID);
	virtual Matrix34 const& GetAuxAudioProxyOffset(TAudioProxyID const nAudioProxyLocalID = DEFAULT_AUDIO_PROXY_ID);
	virtual void PlayFile(char const* const _szFile, TAudioProxyID const _audioProxyLocalID = DEFAULT_AUDIO_PROXY_ID, SAudioCallBackInfo const& _callBackInfo = SAudioCallBackInfo::GetEmptyObject());
	virtual void StopFile(char const* const _szFile, TAudioProxyID const _audioProxyLocalID = DEFAULT_AUDIO_PROXY_ID);
	virtual bool ExecuteTrigger(TAudioControlID const nTriggerID, ELipSyncMethod const eLipSyncMethod, TAudioProxyID const nAudioProxyLocalID = DEFAULT_AUDIO_PROXY_ID, SAudioCallBackInfo const& callBackInfo = SAudioCallBackInfo::GetEmptyObject());
	virtual void StopTrigger(TAudioControlID const nTriggerID, TAudioProxyID const nAudioProxyLocalID = DEFAULT_AUDIO_PROXY_ID);
	virtual void SetSwitchState(TAudioControlID const nSwitchID, TAudioSwitchStateID const nStateID, TAudioProxyID const nAudioProxyLocalID = DEFAULT_AUDIO_PROXY_ID);
	virtual void SetRtpcValue(TAudioControlID const nRtpcID, float const fValue, TAudioProxyID const nAudioProxyLocalID = DEFAULT_AUDIO_PROXY_ID);
	virtual void SetObstructionCalcType(EAudioObjectObstructionCalcType const eObstructionType, TAudioProxyID const nAudioProxyLocalID = DEFAULT_AUDIO_PROXY_ID);
	virtual void SetEnvironmentAmount(TAudioEnvironmentID const nEnvironmentID, float const fAmount, TAudioProxyID const nAudioProxyLocalID = DEFAULT_AUDIO_PROXY_ID);
	virtual void SetCurrentEnvironments(TAudioProxyID const nAudioProxyLocalID = DEFAULT_AUDIO_PROXY_ID);
	virtual void AuxAudioProxiesMoveWithEntity(bool const bCanMoveWithEntity);
	virtual void AddAsListenerToAuxAudioProxy(TAudioProxyID const nAudioProxyLocalID, void (*func)(SAudioRequestInfo const* const), EAudioRequestType requestType = eART_AUDIO_ALL_REQUESTS, TATLEnumFlagsType specificRequestMask = ALL_AUDIO_REQUEST_SPECIFIC_TYPE_FLAGS);
	virtual void RemoveAsListenerFromAuxAudioProxy(TAudioProxyID const nAudioProxyLocalID, void (*func)(SAudioRequestInfo const* const));
	// ~IEntityAudioProxy

private:

	enum EEntityAudioProxyFlags : TATLEnumFlagsType
	{
		eEAPF_NONE                 = 0,
		eEAPF_CAN_MOVE_WITH_ENTITY = BIT(0),
	};

	struct SAudioProxyWrapper
	{
		SAudioProxyWrapper(IAudioProxy* const _pIAudioProxy)
			: pIAudioProxy(_pIAudioProxy)
			, offset(IDENTITY)
		{}

		~SAudioProxyWrapper() {}

		IAudioProxy* const pIAudioProxy;
		Matrix34 offset;

	private:

		SAudioProxyWrapper()
		: pIAudioProxy(NULL)
		, offset(IDENTITY)
		{}
	};

	typedef std::pair<TAudioProxyID const, SAudioProxyWrapper> TAudioProxyPair;
	typedef std::map<TAudioProxyID const, SAudioProxyWrapper, std::less<TAudioProxyID>, stl::STLPoolAllocator<TAudioProxyPair > >	TAuxAudioProxies;
	static TAudioProxyPair s_nullAudioProxyPair;
	static CAudioObjectTransformation s_audioListenerLastTransformation;

	void OnListenerEnter(IEntity const* const pEntity);
	void OnListenerMoveNear(Vec3 const& closestPointToArea);
	void OnListenerMoveInside(Vec3 const& listenerPos);
	void OnListenerExclusiveMoveInside(IEntity const* const __restrict pEntity, IEntity const* const __restrict pAreaHigh, IEntity const* const __restrict pAreaLow, float const fFade);
	void OnMove();
	void OnEnter(IEntity const* const pEntity);
	void OnLeaveNear(IEntity const* const pEntity);
	void OnMoveNear(IEntity const* const __restrict pINearMovingEntity, float const environmentAmount);
	void OnExclusiveMoveInside(IEntity const* const __restrict pEntity, IEntity const* const __restrict pEntityAreaHigh, IEntity const* const __restrict pEntityAreaLow, float const fFade);
	void OnAreaCrossing(IEntity const* const pIEntity);
	TAudioProxyPair& GetAuxAudioProxyPair(TAudioProxyID const nAudioProxyLocalID);
	void SetEnvironmentAmountInternal(IEntity const* const pEntity, float const fAmount) const;

	// Function objects
	struct SReleaseAudioProxy
	{
		SReleaseAudioProxy() {}

		inline void operator()(TAudioProxyPair const& pair)
		{
			pair.second.pIAudioProxy->Release();
		}
	};

	struct SRepositionAudioProxy
	{
		SRepositionAudioProxy(Matrix34 const& _position)
			: position(_position)
		{}

		inline void operator()(TAudioProxyPair const& pair)
		{
			pair.second.pIAudioProxy->SetPosition(position * pair.second.offset);
		}

	private:

		Matrix34 const& position;
	};

	struct SPlayFile
	{
		SPlayFile(char const* const _szFile, SAudioCallBackInfo const& _callBackInfo = SAudioCallBackInfo::GetEmptyObject())
			: szFile(_szFile)
			, callBackInfo(_callBackInfo)
		{}

		inline void operator()(TAudioProxyPair const& pair)
		{
			pair.second.pIAudioProxy->PlayFile(szFile, callBackInfo);
		}

	private:

		char const* const szFile;
		SAudioCallBackInfo const& callBackInfo;
	};

	struct SStopFile
	{
		SStopFile(char const* const _szFile)
			: szFile(_szFile)
		{}

		inline void operator()(TAudioProxyPair const& pair)
		{
			pair.second.pIAudioProxy->StopFile(szFile);
		}

	private:

		char const* const szFile;
	};

	struct SStopTrigger
	{
		SStopTrigger(TAudioControlID const _triggerID)
			: triggerID(_triggerID)
		{}

		inline void operator()(TAudioProxyPair const& pair)
		{
			pair.second.pIAudioProxy->StopTrigger(triggerID);
		}

	private:
		
		TAudioControlID const triggerID;
	};

	struct SSetSwitchState
	{
		SSetSwitchState(TAudioControlID const _switchID, TAudioSwitchStateID const _stateID)
			: switchID(_switchID)
			, stateID(_stateID)
		{}

		inline void operator()(TAudioProxyPair const& pair)
		{
			pair.second.pIAudioProxy->SetSwitchState(switchID, stateID);
		}

	private:

		TAudioControlID const switchID;
		TAudioSwitchStateID const stateID;
	};

	struct SSetRtpcValue
	{
		SSetRtpcValue(TAudioControlID const _rtpcID, float const _value)
			: rtpcID(_rtpcID)
			, value(_value)
		{}

		inline void operator()(TAudioProxyPair const& pair)
		{
			pair.second.pIAudioProxy->SetRtpcValue(rtpcID, value);
		}

	private:

		TAudioControlID const rtpcID;
		float const value;
	};

	struct SSetObstructionCalcType
	{
		SSetObstructionCalcType(EAudioObjectObstructionCalcType const _obstructionType)
			: obstructionType(_obstructionType)
		{}

		inline void operator()(TAudioProxyPair const& pair)
		{
			pair.second.pIAudioProxy->SetObstructionCalcType(obstructionType);
		}

	private:

		EAudioObjectObstructionCalcType const obstructionType;
	};

	struct SSetEnvironmentAmount
	{
		SSetEnvironmentAmount(TAudioEnvironmentID const _environmentID, float const _amount)
			: environmentID(_environmentID)
			, amount(_amount)
		{}

		inline void operator()(TAudioProxyPair const& pair)
		{
			pair.second.pIAudioProxy->SetEnvironmentAmount(environmentID, amount);
		}

	private:

		TAudioEnvironmentID const environmentID;
		float const amount;
	};

	struct SSetCurrentEnvironments
	{
		SSetCurrentEnvironments(EntityId const entityId)
		: m_entityId(entityId)
		{}

		inline void operator()(TAudioProxyPair const& pair)
		{
			pair.second.pIAudioProxy->SetCurrentEnvironments(m_entityId);
		}

	private:

		EntityId const m_entityId;
	};

	struct SSetAuxAudioProxyOffset
	{
		SSetAuxAudioProxyOffset(Matrix34 const& _offset, Matrix34 const& _entityPosition)
		: offset(_offset)
		, entityPosition(_entityPosition)
		{}

		inline void operator()(TAudioProxyPair& pair)
		{
			pair.second.offset = offset;
			(SRepositionAudioProxy(entityPosition))(pair);
		}

	private:

		Matrix34 const& offset;
		Matrix34 const& entityPosition;
	};
	// ~Function objects

	TAuxAudioProxies    m_mapAuxAudioProxies;
	TAudioProxyID       m_audioProxyIDCounter;

	TAudioEnvironmentID m_audioEnvironmentID;
	CEntity*            m_pEntity;
	TATLEnumFlagsType   m_flags;

	float               m_fadeDistance;
	float               m_environmentFadeDistance;
	float               m_lastSetEnvironmentAmount;
};
